json.array!(@receipt_periods) do |receipt_period|
  json.extract! receipt_period, :id, :start_date, :end_date, :accompliched, :user_id
  json.url receipt_period_url(receipt_period, format: :json)
end
