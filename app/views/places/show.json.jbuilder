json.extract! @place, :id, :name, :address, :complement, :latitude, :longitude, :user_id, :created_at, :updated_at
