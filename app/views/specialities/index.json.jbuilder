json.array!(@specialities) do |speciality|
  json.extract! speciality, :id, :name,:home_care,:image_url
  json.url speciality_url(speciality, format: :json)
end
