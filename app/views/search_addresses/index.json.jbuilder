json.array!(@search_addresses) do |search_address|
  json.extract! search_address, :id, :address, :complement, :latitude, :longitude, :user_id
  json.url search_address_url(search_address, format: :json)
end
