json.array!(@attendence_types) do |attendence_type|
  json.extract! attendence_type, :id, :name
  json.url attendence_type_url(attendence_type, format: :json)
end
