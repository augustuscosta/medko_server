json.array!(@attendences) do |attendence|
  json.extract! attendence,:value, :id, :date,:speciality_id, :place_id, :search_address_id, :service_window_id, :payment_method_id,:health_plan_id,:payment_id, :patient_id, :doctor_id
  json.url attendence_url(attendence, format: :html)
end
