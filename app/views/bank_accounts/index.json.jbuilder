json.array!(@bank_accounts) do |bank_account|
  json.extract! bank_account, :id, :bank, :full_name, :cpf_cnpj, :account, :operation, :user
  json.url bank_account_url(bank_account, format: :json)
end
