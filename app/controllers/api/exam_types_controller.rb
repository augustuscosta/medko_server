class Api::ExamTypesController < Api::BaseController


  respond_to :json
  
  def index
    @exam_types = ExamType.all
    render 'exam_types/index.json.jbuilder'
  end

end