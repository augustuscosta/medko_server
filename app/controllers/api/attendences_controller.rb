class Api::AttendencesController < Api::BaseController

  before_filter :authenticate_user!

  respond_to :json

  def index
     if current_user.account_type_id == 1
        @attendences = Attendence.where(patient_id: current_user).order("created_at DESC")
          render 'attendences/index_api.json.jbuilder'
        end

     if current_user.account_type_id == 3
          @attendences = Attendence.where(doctor_id: current_user).order("created_at DESC")
          render 'attendences/index_api.json.jbuilder'
      end
  end

  def payment
    @attendence = Attendence.find(params[:id])
    render json: {url: @attendence.payment_request},status:200
  end

 def create_attendences
        @service_windows = ServiceWindow.find_by_id(params['id'])
        @attendence = Attendence.new
        @attendence.place_id = @service_windows.place_id
        @attendence.service_window_id = @service_windows.id
        @attendence.doctor_id = @service_windows.user_id
        @attendence.health_plan_id = params['health']
        @attendence.exam_type_id = @service_windows.exam_type_id
        @attendence.payment_method_id = params['payment']
        @attendence.speciality_id = @service_windows.speciality_id
        @attendence.attendence_type_id = 2
        @attendence.value = @service_windows.value
        @attendence.attendence_status_id = 7
        @attendence.patient_id = current_user.id
        @attendence.save

        render 'attendences/show_api.json.jbuilder'    

        return @attendence
     end

   def search_attendences_clinic
      @speciality_id = params['speciality']
      @exam_type_id = params['exam']
      @payment_id = params['payment']
      @health_plan_id = params['health_plan']
      @attendence_type_id = params['attendence_type']
      @latitude = params['latitude']
      @longitude = params['longitude'] 
      @patient_id = current_user.id
      @service_windows  = Attendence.search_clinic(@exam_type_id,@payment_id,@speciality_id,@health_plan_id,@latitude,@longitude,@patient_id)
      render 'service_windows/index.json.jbuilder'
   end

  def search_attendences_home_care
      @speciality_id = params['speciality']
      @exam_type_id = params['exam']
      @payment_id = params['payment']
      @attendence_type_id = params['attendence_type']
      @latitude = params['latitude']
      @longitude = params['longitude'] 
      @address = params['address']
      @patient_id = current_user.id

    @attendence = Attendence.search_home_care(@address,@latitude,@longitude,@exam_type_id,@speciality_id,@patient_id)
      render 'attendences/show_api.json.jbuilder'
   end

    def get_payment_method
      @payment_method = PaymentMethod.all
    end

    def get_health_plan
      @health_plan = HealthPlan.all
    end

    def get_attendence_type
      @attendence_type = AttendenceType.all
    end

  def show
    @attendence = Attendence.find(params['id'])
    render 'attendences/show_api.json.jbuilder'
  end

   def confirm
     @attendence = Attendence.find(params['attendence']['id'])
     @attendence.attendence_status_id = 2
     @attendence.save
     render json: {nothing: true}
   end

    

    def cancel
      @attendence = Attendence.find(params['attendence']['id'])
      @attendence.attendence_status_id = 3
      @attendence.save  
      render json: {nothing: true}
    end

     def time
      attendence = Attendence.find(params['attendence']['id'])
      attendence.date = params['attendence']['date']
      attendence.attendence_status_id = 1
      attendence.save  
      render json: {nothing: true}
    end

end

