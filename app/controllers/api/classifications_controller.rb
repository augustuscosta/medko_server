class Api::ClassificationsController < Api::BaseController
  before_filter :authenticate_user!
  respond_to :json 
  
    def create
	    @classification = Classification.new(classification_params)
	    @classification.user = current_user
	    @classification.value = params['classification']['value']
	    @classification.attendence_id = params['attendence_id']
    
    if @classification.save
         @attendence = Attendence.find(params['attendence_id'])
         @attendence.classification_id = @classification.id
         @attendence.save
      end
       render json: {nothing: true}   
  end

  def classification_params
      params.require(:classification).permit(:user_id, :attendence_id, :value)
  end

end