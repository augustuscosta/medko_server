class Api::SpecialitiesController < Api::BaseController


  respond_to :json
  
  def index
    @specialities = Speciality.all
    render 'specialities/index.json.jbuilder'

  end

end