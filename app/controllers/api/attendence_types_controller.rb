class Api::AttendenceTypesController < Api::BaseController


  respond_to :json
  
  def index
    @attendence_types = AttendenceType.all
    render 'attendence_types/index.json.jbuilder'

  end

end