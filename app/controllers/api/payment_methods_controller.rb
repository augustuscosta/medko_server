class Api::PaymentMethodsController < Api::BaseController


  respond_to :json
  
  def index
    @payment_methods = PaymentMethod.all
    render 'payment_methods/index.json.jbuilder'

  end

end