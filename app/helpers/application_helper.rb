module ApplicationHelper


  def render_stars(value)
    output = ''
    if (1..5).include?(value.floor)
      value.floor.times { output += image_tag('star-on.png')}
    end
    output.html_safe
  end

  def number_to_currency_br(price)
  number_to_currency(price, :unit => "R$ ", :separator => ",", :delimiter => ".")
end


end
