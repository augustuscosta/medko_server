class ReceiptPeriod < ActiveRecord::Base
  belongs_to :user
  has_one :payment
end
