class BankAccount < ActiveRecord::Base
	belongs_to :user

	validates :bank,:presence => { :message => "^Banco não pode estar em branco"}
	validates :full_name,:presence => { :message => "^Digite seu nome completo"}
	validates :cpf_cnpj,:presence => { :message => "^Digite seu cpf ou cnpj"},:numericality => { :message => "^Digite apenas números no seu cpf ou cnpj"}
	validates :account,:presence => { :message => "^Digite o numero da sua conta"}
	validates :agency,:presence => { :message => "^Digite o numero da sua agencia"}
	
end
