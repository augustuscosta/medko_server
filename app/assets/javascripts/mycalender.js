$(document).ready(function() {
   $("#calendar").fullCalendar({
   	monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'], 
   	monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'], 
   	dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'], 
   	dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
   	buttonText: { today: 'hoje',  month: 'mês',   week: 'semana', day: 'dia'  },  
   	columnFormat: { month: 'ddd',   week: 'ddd d/M',    day: 'dddd d/M' }, 
    timezoneParam: 'America/Argentina/Buenos_Aires',
   	allDayText: 'dia todo', 
   	axisFormat: 'H:mm',
    ignoreTimezone: false,
    timeFormat:'H:mm',
    agenda: 'H:mm{ - H:mm}',
    events: "/attendences.json"

});
});





  

  
