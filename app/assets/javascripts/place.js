var geocoder;
var map;

//Inicia o mapa usando a localizacao do usuario
  function initializeMap() {
    var options = {
        zoom: 5,
        scrollwheel: false,
        disableDoubleClickZoom: true, // <---
        panControl: false,
        streetViewControl: false
    };

    map = new google.maps.Map(document.getElementById('mapa'), options);
    geocoder = new google.maps.Geocoder();

    if ($('#latitude').val() != 0 && $('#longitude').val() != 0){
        latitude = Number($('#latitude').val());
        longitude = Number($('#longitude').val());
        var edit = new google.maps.LatLng(latitude, longitude);
        map.setCenter(edit);
        map.setZoom(18);
       } else{
       if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var ponto = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                map.setCenter(ponto);
                map.setZoom(18);
            });
        } 
        else { //se o usuario nao autorizar a localizacao ele ira para essa cordenada
            var latlng = new google.maps.LatLng( -3.7913568716114554, -38.51927105000004);
        }
     } 
    //arrastar o mapa e gerar latitude longitude
    google.maps.event.addListener(map, 'dragend', function () {
        var latitude = map.getCenter().lat()
        var longitude = map.getCenter().lng()
        $('#latitude').val(latitude);
        $('#longitude').val(longitude);
    });
  }

 $(document).ready(function() {
    initializeMap();
    //permite que um endereço digitado pelo usuario sejá marcado o mapa
    function carregarNoMapa(endereco) {
    geocoder.geocode({ 'address': endereco + ', Brasil', 'region': 'BR' }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          var latitude = results[0].geometry.location.lat();
          var longitude = results[0].geometry.location.lng();
    
          $('#Endereco').val(results[0].formatted_address);
          $('#latitude').val(latitude);
                    $('#longitude').val(longitude);
              //gera o maker e o zoom do mapa
          var location = new google.maps.LatLng(latitude, longitude);
          marker.setPosition(location);
          map.setCenter(location);
          map.setZoom(18);
          
        }
      }
    })
  }
    //faz com que ao digitar um endereco o usario receba sujestoes para autocomplete
   $("#Endereco").autocomplete({
    source: function (request, response) {
      geocoder.geocode({ 'address': request.term + ', Brasil', 'region': 'BR' }, function (results, status) {
        response($.map(results, function (item) {
          return {
            label: item.formatted_address,
            value: item.formatted_address,
            latitude: item.geometry.location.lat(),
                  longitude: item.geometry.location.lng()
          }
        }));
      })
    },
    select: function (event, ui) {
      $("#latitude").val(ui.item.latitude);
        $("#longitude").val(ui.item.longitude);
      var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
      map.setCenter(location);
      map.setZoom(18);
    }
  });
});
