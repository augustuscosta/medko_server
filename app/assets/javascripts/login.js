
  $(document).ready(function() {
            $(".cont_tab").hide();
            $("ul.tabs lu:first").addClass("active").show();
            $(".cont_tab:first").show();
            $("ul.tabs lu").click(function () {
                $("ul.tabs lu").removeClass("active")
                $(this).addClass("active")
                $(".cont_tab").hide();
                var activeTab = $(this).find("a").attr("href");
                $(activeTab).fadeIn("fast");
                return false;
        });
    });

  
  $(document).ready(function() {
            $(".cont_tab").hide();
            $("ul.tabs_med lu:first").addClass("active").show();
            $(".cont_tab:first").show();
            $("ul.tabs_med lu").click(function () {
                $("ul.tabs_med lu").removeClass("active")
                $(this).addClass("active")
                $(".cont_tab").hide();
                var activeTab = $(this).find("a").attr("href");
                $(activeTab).fadeIn("fast");
                return false;
        });
    });

/* imagem do avatar */

         function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#avatar_show')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }