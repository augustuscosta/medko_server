class ApplicationMailer < ActionMailer::Base
  default from: "medkoapp@gmail.com"
  layout 'mailer'
end
