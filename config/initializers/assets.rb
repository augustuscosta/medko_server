# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

Rails.application.config.assets.precompile += %w(paciente_cadastro.css)

Rails.application.config.assets.precompile += %w(medico_cadastro.css)

Rails.application.config.assets.precompile += %w(menu.css)

Rails.application.config.assets.precompile += %w( places.css )

Rails.application.config.assets.precompile += %w( show.css )

Rails.application.config.assets.precompile += %w( service_windows.css )

Rails.application.config.assets.precompile += %w( attendences.css )

Rails.application.config.assets.precompile += %w( jRating.jquery.js )

Rails.application.config.assets.precompile += %w( specialities.css )

Rails.application.config.assets.precompile += %w( search.css )

Rails.application.config.assets.precompile += %w( place.js )

Rails.application.config.assets.precompile += %w( attendences.js )

Rails.application.config.assets.precompile += %w( payments.css )

Rails.application.config.assets.precompile += %w( show_medico.css )

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
