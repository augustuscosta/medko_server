# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


 	AccountType.create( name: 'paciente')
   	AccountType.create( name: 'administrador')
    AccountType.create( name: 'médico')

    AttendenceType.create( name: 'Domicílio')
    AttendenceType.create( name: 'Consultório')

	Speciality.create(name:'Acupuntura')
	Speciality.create(name:'Alergia e imunologia')
	Speciality.create(name:'Anestesiologia')
	Speciality.create(name:'Angiologia')
	Speciality.create(name:'Cancerologia')
	Speciality.create(name:'Cardiologia', home_care:true)
	Speciality.create(name:'Cirurgia cardiovascular')
	Speciality.create(name:'Cirurgia da mão')
	Speciality.create(name:'Cirurgia de cabeça e pescoço')
	Speciality.create(name:'Cirurgia do aparelho digestivo')
	Speciality.create(name:'Cirurgia geral')
	Speciality.create(name:'Cirurgia pediátrica')
	Speciality.create(name:'Cirurgia plástica')
	Speciality.create(name:'Cirurgia torácica')
	Speciality.create(name:'Cirurgia vascular')
	Speciality.create(name:'Clínica médica', home_care:true)
	Speciality.create(name:'Coloproctologia')
	Speciality.create(name:'Endocrinologia e metabologia')
	Speciality.create(name:'Endoscopia')
	Speciality.create(name:'Gastroenterologia')
	Speciality.create(name:'Genética médica')
	Speciality.create(name:'Geriatria', home_care:true)
	Speciality.create(name:'Ginecologia e obstetrícia')
	Speciality.create(name:'Hematologia e hemoterapia')
	Speciality.create(name:'Homeopatia')
	Speciality.create(name:'Mastologia')
	Speciality.create(name:'Medicina de emergência')
	Speciality.create(name:'Medicina de família e comunidade')
	Speciality.create(name:'Medicina do trabalho')
	Speciality.create(name:'Medicina de tráfego')
	Speciality.create(name:'Medicina esportiva')
	Speciality.create(name:'Medicina física e reabilitação')
	Speciality.create(name:'Medicina intensiva')
	Speciality.create(name:'Medicina legal e perícia médica')
	Speciality.create(name:'Medicina nuclear')
	Speciality.create(name:'Medicina preventiva e social')
	Speciality.create(name:'Nefrologia')
	Speciality.create(name:'Neurocirurgia')
	Speciality.create(name:'Neurologia', home_care:true)
	Speciality.create(name:'Nutrologia')
	Speciality.create(name:'Oftalmologia')
	Speciality.create(name:'Ortopedia e traumatologia')
	Speciality.create(name:'Otorrinolaringologia', home_care:true)
	Speciality.create(name:'Patologia')
	Speciality.create(name:'Patologia clínica/medicina laboratorial')
	Speciality.create(name:'Pediatria', home_care:true)
	Speciality.create(name:'Pneumologia')
	Speciality.create(name:'Psiquiatria')
	Speciality.create(name:'Radiologia e diagnóstico por imagem')
	Speciality.create(name:'Radioterapia')
	Speciality.create(name:'Reumatologia')
	Speciality.create(name:'Urologia')




	ExamType.create(name:'Ultrassonografia abdominal',home_care:true)
	ExamType.create(name:'Ultrassonografia de vias urinárias',home_care:true)
	ExamType.create(name:'Ultrassonografia transvaginal',home_care:true)
	ExamType.create(name:'Ultrassonografia de partes moles',home_care:true)
	ExamType.create(name:'Ultrassonografia de articulações',home_care:true)
	ExamType.create(name:'Ultrassonografia pélvica',home_care:true)
	ExamType.create(name:'Ecocardiograma  bidimensional')
	ExamType.create(name:'Eletroneuromiografia')
	ExamType.create(name:'Eletroencefalograma')
	ExamType.create(name:'Espirometria')
	ExamType.create(name:'Eletrocardiograma')
	ExamType.create(name:'Potencial evocado')
	ExamType.create(name:'Laringoscopia')
	ExamType.create(name:'Endoscopia Digestiva Alta')
	ExamType.create(name:'Colonoscopia')
	ExamType.create(name:'Retossigmoidoscopia')


	HealthPlan.create(name:'Allianz Saúde')
	HealthPlan.create(name:'Amil')
	HealthPlan.create(name:'Assefaz')
	HealthPlan.create(name:'Bradesco Saúde')
	HealthPlan.create(name:'Camed')
	HealthPlan.create(name:'Cassi')
	HealthPlan.create(name:'Gama Saúde')
	HealthPlan.create(name:'Geap')
	HealthPlan.create(name:'Golden Cross')
	HealthPlan.create(name:'Hapvida')
	HealthPlan.create(name:'Intermédica')
	HealthPlan.create(name:'Medial Saúde')
	HealthPlan.create(name:'Porto Seguro')
	HealthPlan.create(name:'Sul América')
	HealthPlan.create(name:'Unimed')

	PaymentMethod.create(name:'Cartão de crédito')
	PaymentMethod.create(name:'Cartão de débito')
	PaymentMethod.create(name:'Dinheiro')
	PaymentMethod.create(name:'Cheque')


	User.create(avatar:File.new("app/assets/images/medicofoto.jpg"),zip_code: '60345662', address: 'Rua,Sampaio - 345 -Vila Velha ,Fortaleza', name:'Lucas Costa',date_location:'2016-10-04 17:00', latitude:'-3.7253947',longitude:'-38.553352399999994',account_type_id: '3',active:'true',crm:'4403',value_home_care: '200',telephone:'88584451',email: 'lucas@gmail.com',password:'medico123',bank_account_attributes: [bank:'bradesco',full_name:'Jonhatan Costa Morais',cpf_cnpj:'05727843332',account:'empresa',agency:'223'])

	User.create(avatar:File.new("app/assets/images/medicafoto.jpg"),name:'Beatriz Sousa',date_location:'2016-10-04 17:00', latitude:'-3.7160818',longitude:'-38.553352399999994',account_type_id: '3',active:'true',value_home_care: '300', crm:'4402',telephone:'88484432',email: 'bea@gmail.com',password:'medico123',bank_account_attributes: [bank:'bradesco',full_name:'Amelia Sousa Azevedo',cpf_cnpj:'1444',account:'empresa',operation:'bla',agency:'223'])
	
	User.create(avatar:File.new("app/assets/images/anafoto.jpg"),name:'Ana Cecilia',account_type_id: '1',active:'true',telephone:'88833267',email: 'ana@gmail.com', password:'paciente123')

	User.create(avatar:File.new("app/assets/images/robertfoto.jpg"),name:'Robert Stark',account_type_id: '1',active:'true',telephone:'88833245',email: 'tony@gmail.com', password:'paciente123')

	User.create(avatar:File.new("app/assets/images/eye.png"),name:'Pedro Lucas',account_type_id: '2',active:'true',telephone:'888332',email: 'administrador@gmail.com', password:'administrador123')
	
	SearchAddress.create(address: 'R. Brasil - Vila Isabel Eber, Jundiaí - SP, 13202-284, Brasil', complement:'altos',user_id:'3')

	Place.create(name:'Clinica 1', address:"R. Rosinha Sampaio, Fortaleza - CE, 60345-662",complement:'baixo', user_id:'1',latitude:'-3.7243513',longitude:'-38.5966894')
	
	Place.create(name:'Clinica 2', address:"R. Rosinha - Carlito Pamplona, Fortaleza - CE, 60311-440, Brasil",complement:'altos', user_id:'2', latitude:'-3.7160818',longitude:'-38.553352399999994')

	AttendenceStatus.create(name:'Agendado' ,doctor_message: 'Atendimento agendado com sucesso', patient_message: 'Atendimento agendado com sucesso')
	AttendenceStatus.create(name:'Realizado',doctor_message: 'Atendimento realizado', patient_message: 'Atendimento realizado')
	AttendenceStatus.create(name:'Cancelado' ,doctor_message: 'Atendimento cancelado', patient_message: 'Atendimento cancelado')
	AttendenceStatus.create(name:'Aguardando pagamento' ,doctor_message: 'Atendimento aguardando o pagamento', patient_message: 'Atendimento aguardando o pagamento')
	AttendenceStatus.create(name:'Pagamento confirmado' ,doctor_message: 'Atendimento com o pagamento confirmado', patient_message: 'Atendimento com o pagamento confirmado')
	AttendenceStatus.create(name:'Aguardando confirmação médico' ,doctor_message: 'Atendimento confirmado pelo médico', patient_message: 'Atendimento confirmado pelo médico')
	AttendenceStatus.create(name:'Aguardando confirmação paciente' ,doctor_message: 'Atendimento confirmado pelo paciente', patient_message: 'Atendimento confirmado pelo paciente')

	ServiceWindow.create(user_id: '1',week_day:'5',health_plan_ids:'2',start_time:'07:00',end_time: '12:00',exam_type_id:'2',speciality_id:'3',service_time:'30',place_id:'2',value:'300')
	
	ServiceWindow.create(user_id: '2',week_day:'5',health_plan_ids:'2',start_time:'09:00',end_time: '14:00',exam_type_id:'2',speciality_id:'1',service_time:'20',place_id:'1',value:'150')

	ServiceWindow.create(user_id: '1',week_day:'5',payment_method_ids:'1',start_time:'14:30',end_time: '17:00',exam_type_id:'2',speciality_id:'1',service_time:'20',place_id:'1',value:'1250')

	Attendence.create(date:'2016-09-22 13:30',place_id:'1',patient_id: '3',service_window_id:'1',doctor_id: '1',payment_method_id:'3',health_plan_id:'3',attendence_type_id: '2',exam_type_id:'2',speciality_id:'1',attendence_status_id:'1')

	Attendence.create(date:'2016-09-22 14:00',place_id: '2',patient_id: '3', doctor_id: '1',service_window_id:'1',payment_method_id:'2',attendence_type_id: '2', attendence_status_id:'2',exam_type_id:'3',speciality_id:'3')
	
	Attendence.create(date:'2016-10-20 14:00',place_id: '2',patient_id: '3', doctor_id: '1',service_window_id:'1',payment_method_id:'2',attendence_type_id: '2', attendence_status_id:'2',exam_type_id:'3',speciality_id:'3',value:'240')

	Attendence.create(date:'2016-11-02 14:00',patient_id: '3',search_address_id: '1', doctor_id: '1',attendence_type_id: '1', attendence_status_id:'2',exam_type_id:'3',value:'240')



