class CreatePaymentMethodsServiceWindows < ActiveRecord::Migration
  def change
    create_table :payment_methods_service_windows do |t|
      t.references :service_window, index: true, foreign_key: true
      t.references :payment_method, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
