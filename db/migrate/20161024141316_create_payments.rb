class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.float :price
      t.float :receipt
      t.references :payment_period, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :receipt_period, index: true, foreign_key: true
    
      t.timestamps null: false
    end
  end
end
