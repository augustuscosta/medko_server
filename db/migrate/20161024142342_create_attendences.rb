class CreateAttendences < ActiveRecord::Migration
  def change
    create_table :attendences do |t|
      t.datetime :date
      t.boolean :home_care
      t.float :value
      t.references :exam_type, index: true, foreign_key: true
      t.references :speciality, index: true, foreign_key: true
      t.references :place, index: true, foreign_key: true
      t.references :search_address, index: true, foreign_key: true
      t.references :payment_method, index: true, foreign_key: true
      t.references :attendence_status, index: true, foreign_key: true
      t.references :attendence_type, index: true, foreign_key: true
      t.references :payment, index: true, foreign_key: true
      t.references :health_plan, index: true,foreign_key: true
      t.integer :patient_id
      t.integer :doctor_id

      t.timestamps null: false
    end
  end
end
