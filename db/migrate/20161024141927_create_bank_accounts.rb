class CreateBankAccounts < ActiveRecord::Migration
  def change
    create_table :bank_accounts do |t|
      t.string :bank
      t.string :full_name
      t.string :cpf_cnpj
      t.string :account
      t.string :agency
      t.string :operation
      t.references :user, index: true,foreign_key: true

      t.timestamps null: false
    end
  end
end
