class AddAttachmentImageUrlToExamTypes < ActiveRecord::Migration
  def self.up
    change_table :exam_types do |t|
      t.attachment :image_url
    end
  end

  def self.down
    remove_attachment :exam_types, :image_url
  end
end
