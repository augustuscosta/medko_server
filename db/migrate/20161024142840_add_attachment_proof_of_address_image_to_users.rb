class AddAttachmentProofOfAddressImageToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :proof_of_address_image
    end
  end

  def self.down
    remove_attachment :users, :proof_of_address_image
  end
end
