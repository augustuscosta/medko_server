class CreatePaymentPeriods < ActiveRecord::Migration
  def change
    create_table :payment_periods do |t|
      t.date :start_date
      t.date :end_date
      t.float :value
      t.boolean :accomplished

      t.timestamps null: false
    end
  end
end
