class CreateReceiptPeriods < ActiveRecord::Migration
  def change
    create_table :receipt_periods do |t|
      t.date :start_date
      t.date :end_date
      t.float :value
      t.boolean :accompliched
     
      t.timestamps null: false
    end
  end
end
