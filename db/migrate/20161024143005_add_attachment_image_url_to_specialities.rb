class AddAttachmentImageUrlToSpecialities < ActiveRecord::Migration
  def self.up
    change_table :specialities do |t|
      t.attachment :image_url
    end
  end

  def self.down
    remove_attachment :specialities, :image_url
  end
end
