require 'test_helper'

class ReceiptPeriodsControllerTest < ActionController::TestCase
  setup do
    @receipt_period = receipt_periods(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:receipt_periods)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create receipt_period" do
    assert_difference('ReceiptPeriod.count') do
      post :create, receipt_period: { accompliched: @receipt_period.accompliched, end_date: @receipt_period.end_date, start_date: @receipt_period.start_date, user_id: @receipt_period.user_id }
    end

    assert_redirected_to receipt_period_path(assigns(:receipt_period))
  end

  test "should show receipt_period" do
    get :show, id: @receipt_period
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @receipt_period
    assert_response :success
  end

  test "should update receipt_period" do
    patch :update, id: @receipt_period, receipt_period: { accompliched: @receipt_period.accompliched, end_date: @receipt_period.end_date, start_date: @receipt_period.start_date, user_id: @receipt_period.user_id }
    assert_redirected_to receipt_period_path(assigns(:receipt_period))
  end

  test "should destroy receipt_period" do
    assert_difference('ReceiptPeriod.count', -1) do
      delete :destroy, id: @receipt_period
    end

    assert_redirected_to receipt_periods_path
  end
end
