require 'test_helper'

class PaymentPeriodsControllerTest < ActionController::TestCase
  setup do
    @payment_period = payment_periods(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:payment_periods)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create payment_period" do
    assert_difference('PaymentPeriod.count') do
      post :create, payment_period: { accomplished: @payment_period.accomplished, end_date: @payment_period.end_date, start_date: @payment_period.start_date }
    end

    assert_redirected_to payment_period_path(assigns(:payment_period))
  end

  test "should show payment_period" do
    get :show, id: @payment_period
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @payment_period
    assert_response :success
  end

  test "should update payment_period" do
    patch :update, id: @payment_period, payment_period: { accomplished: @payment_period.accomplished, end_date: @payment_period.end_date, start_date: @payment_period.start_date }
    assert_redirected_to payment_period_path(assigns(:payment_period))
  end

  test "should destroy payment_period" do
    assert_difference('PaymentPeriod.count', -1) do
      delete :destroy, id: @payment_period
    end

    assert_redirected_to payment_periods_path
  end
end
