require 'test_helper'

class ServiceWindowsControllerTest < ActionController::TestCase
  setup do
    @service_window = service_windows(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:service_windows)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create service_window" do
    assert_difference('ServiceWindow.count') do
      post :create, service_window: { end_time: @service_window.end_time, place_id: @service_window.place_id, service_time: @service_window.service_time, start_time: @service_window.start_time, user_id: @service_window.user_id, value: @service_window.value, week_day: @service_window.week_day }
    end

    assert_redirected_to service_window_path(assigns(:service_window))
  end

  test "should show service_window" do
    get :show, id: @service_window
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @service_window
    assert_response :success
  end

  test "should update service_window" do
    patch :update, id: @service_window, service_window: { end_time: @service_window.end_time, place_id: @service_window.place_id, service_time: @service_window.service_time, start_time: @service_window.start_time, user_id: @service_window.user_id, value: @service_window.value, week_day: @service_window.week_day }
    assert_redirected_to service_window_path(assigns(:service_window))
  end

  test "should destroy service_window" do
    assert_difference('ServiceWindow.count', -1) do
      delete :destroy, id: @service_window
    end

    assert_redirected_to service_windows_path
  end
end
