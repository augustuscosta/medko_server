require 'test_helper'

class AttendenceTypesControllerTest < ActionController::TestCase
  setup do
    @attendence_type = attendence_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:attendence_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create attendence_type" do
    assert_difference('AttendenceType.count') do
      post :create, attendence_type: { name: @attendence_type.name }
    end

    assert_redirected_to attendence_type_path(assigns(:attendence_type))
  end

  test "should show attendence_type" do
    get :show, id: @attendence_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @attendence_type
    assert_response :success
  end

  test "should update attendence_type" do
    patch :update, id: @attendence_type, attendence_type: { name: @attendence_type.name }
    assert_redirected_to attendence_type_path(assigns(:attendence_type))
  end

  test "should destroy attendence_type" do
    assert_difference('AttendenceType.count', -1) do
      delete :destroy, id: @attendence_type
    end

    assert_redirected_to attendence_types_path
  end
end
