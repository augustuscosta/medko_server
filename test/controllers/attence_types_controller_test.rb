require 'test_helper'

class AttenceTypesControllerTest < ActionController::TestCase
  setup do
    @attence_type = attence_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:attence_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create attence_type" do
    assert_difference('AttenceType.count') do
      post :create, attence_type: { name: @attence_type.name }
    end

    assert_redirected_to attence_type_path(assigns(:attence_type))
  end

  test "should show attence_type" do
    get :show, id: @attence_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @attence_type
    assert_response :success
  end

  test "should update attence_type" do
    patch :update, id: @attence_type, attence_type: { name: @attence_type.name }
    assert_redirected_to attence_type_path(assigns(:attence_type))
  end

  test "should destroy attence_type" do
    assert_difference('AttenceType.count', -1) do
      delete :destroy, id: @attence_type
    end

    assert_redirected_to attence_types_path
  end
end
